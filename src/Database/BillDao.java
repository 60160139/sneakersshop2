/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Panu Rungkaew
 */
public class BillDao {
     public  static boolean insert(int totalcost,int loginId,int memberId){
         Connection conn = Database.connect();
 
         try {
             Statement stm = conn.createStatement();
                     String sql = "INSERT INTO Bill (\n" +
"                       Bill_totalcost,\n" +             
"                       Login_id,\n" +
"                       Member_id\n" +
"                  )\n" +
"                  VALUES (\n" +
"                      '%d',\n" +
"                      '%d',\n" +
"                      '%d'\n" +
"                  );"
                             ;
                     stm.execute(String.format(sql,
                             totalcost,
                             loginId,
                             memberId));
             Database.close();
         } catch (SQLException ex) {
             Logger.getLogger(BillDao.class.getName()).log(Level.SEVERE, null, ex);
         }
          Database.close();
        return true;
     }
     public  static boolean update(Bill bill){
         String sql = "UPDATE Bill\n" +
"   SET \n" +
"       Bill_totalcost = '%s',\n" +
"       Bill_date = '%s',\n" +
"       Login_id = '%s',\n" +
"       Member_id = '%s'\n" +
" WHERE Bill_id = %d ;";
        Connection conn = Database.connect();
        Statement stm;
         try {
             stm = conn.createStatement();
             boolean ret = stm.execute(String.format(sql,bill.getBill_totalcost(),bill.getBill_Date()
             ,bill.getLogin_id(),bill.getMember_id()));
            Database.close();
            return ret;
         } catch (SQLException ex) {
             Logger.getLogger(BillDao.class.getName()).log(Level.SEVERE, null, ex);
         }
          Database.close();
        return true;
     }
        public static boolean delete(Bill bill){
            
        
            String sql = "DELETE FROM Bill\n" +
"                           WHERE Member_id = 2 ";
          Connection conn = Database.connect();
         try {
             Statement stm = conn.createStatement();
             boolean ret = stm.execute(String.format(sql,bill.getBill_id()));
            Database.close();
         } catch (SQLException ex) {
             Logger.getLogger(BillDao.class.getName()).log(Level.SEVERE, null, ex);
         }
         Database.close();
        return true;
        }
        
        
        public static ArrayList<Bill> getBills(){
            ArrayList<Bill> list = new ArrayList<Bill>();
         Connection conn = Database.connect();
         try {
             Statement stm = conn.createStatement();
             String sql = "SELECT Bill_id,\n" +
"                           Bill_totalcost,\n" +
"                           Bill_date,\n" +
"                           Login_id,\n" +
"                           Member_id\n" +
"                           FROM Bill";
             ResultSet rs = stm.executeQuery(sql);
            while(rs.next()){
                //System.out.println(rs.getInt("Staff_id")+" "+rs.getString("Staff_username"));
                Bill bill = toObject(rs);
                list.add(bill);
            }
            Database.close();
            return list;
         } catch (SQLException ex) {
             Logger.getLogger(BillDao.class.getName()).log(Level.SEVERE, null, ex);
         }
         Database.close();
        return null;
        }

    private static Bill toObject(ResultSet rs) throws SQLException {
        Bill bill;
        bill = new Bill();
        bill.setBill_id(rs.getInt("Bill_id"));
        bill.setBill_totalcost(rs.getInt("Bill_totalcost"));
        bill.setBill_Date(rs.getString("Bill_date"));
        bill.setLogin_id(rs.getInt("Login_id"));
        bill.setMember_id(rs.getInt("Member_id"));
        
        
        return bill;
    }
    public static Bill getBill(int id){
        String sql = "SELECT Bill_id,\n" +
"                       Bill_totalcost,\n" +
"                       Bill_date,\n" +
"                       Login_id,\n" +
"                       Member_id\n" +
"                       FROM Bill WHERE Bill_id = %d";
        Connection conn = Database.connect();
         try {
             Statement stm = conn.createStatement();
             ResultSet rs = stm.executeQuery(String.format(sql,id));
              if(rs.next()){
                Bill bill = toObject(rs);
                Database.close();
                return bill;
            }
         } catch (SQLException ex) {
             Logger.getLogger(BillDao.class.getName()).log(Level.SEVERE, null, ex);
         }
           Database.close();
        return null;
    }
}
