/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database;

/**
 *
 * @author Panu Rungkaew
 */
public class BillDetail {
    private int Bucket_id;
    private int Bucket_number;
    private int Bucket_totolcost;
    private int Bill_id;
    private int Product_id;

    public BillDetail() {
        this.Bucket_id = -1;
    }

    
    public BillDetail(int Bucket_id, int Bucket_number, int Bucket_totolcost, int Bill_id, int Product_id) {
        this.Bucket_id = Bucket_id;
        this.Bucket_number = Bucket_number;
        this.Bucket_totolcost = Bucket_totolcost;
        this.Bill_id = Bill_id;
        this.Product_id = Product_id;
    }

    
    public int getBucket_id() {
        return Bucket_id;
    }

    public void setBucket_id(int Bucket_id) {
        this.Bucket_id = Bucket_id;
    }

    public int getBucket_number() {
        return Bucket_number;
    }

    public void setBucket_number(int Bucket_number) {
        this.Bucket_number = Bucket_number;
    }

    public int getBucket_totolcost() {
        return Bucket_totolcost;
    }

    public void setBucket_totolcost(int Bucket_totolcost) {
        this.Bucket_totolcost = Bucket_totolcost;
    }

    public int getBill_id() {
        return Bill_id;
    }

    public void setBill_id(int Bill_id) {
        this.Bill_id = Bill_id;
    }

    public int getProduct_id() {
        return Product_id;
    }

    public void setProduct_id(int Product_id) {
        this.Product_id = Product_id;
    }
@Override
    public String toString() {
        return "Bucket{" + "Bucket_id=" + Bucket_id +  ", Bucket_number=" + Bucket_number + ", Bucket_totolcost=" + Bucket_totolcost + ", Product_id=" + Product_id + ", Bill_id=" + Bill_id +  '}';
    }


}
