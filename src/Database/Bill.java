/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database;

/**
 *
 * @author Panu Rungkaew
 */
public class Bill {
    int Bill_id;
    int Bill_totalcost;
    String Bill_Date;
    int Login_id;
    int Member_id;

    public Bill() {
//        this.Bill_id = -1;
    }

    public Bill(int Bill_id, int Bill_totalcost, String Bill_Date, int Login_id, int Member_id) {
        this.Bill_id = Bill_id;
        this.Bill_totalcost = Bill_totalcost;
        this.Bill_Date = Bill_Date;
        this.Login_id = Login_id;
        this.Member_id = Member_id;
    }

//    Bill() {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
    
    
    
    
    

    public int getBill_id() {
        return Bill_id;
    }

    public void setBill_id(int Bill_id) {
        this.Bill_id = Bill_id;
    }

    public int getBill_totalcost() {
        return Bill_totalcost;
    }

    public void setBill_totalcost(int Bill_totalcost) {
        this.Bill_totalcost = Bill_totalcost;
    }

    public String getBill_Date() {
        return Bill_Date;
    }

    public void setBill_Date(String Bill_Date) {
        this.Bill_Date = Bill_Date;
    }

    public int getLogin_id() {
        return Login_id;
    }

    public void setLogin_id(int Login_id) {
        this.Login_id = Login_id;
    }

    public int getMember_id() {
        return Member_id;
    }

    public void setMember_id(int Member_id) {
        this.Member_id = Member_id;
    }
    
    @Override
    public String toString() {
        return "Bill{" + "Bill_id=" + Bill_id +  ", Bill_totalcost=" + Bill_totalcost + ", Bill_Date=" + Bill_Date + ", Login_id=" + Login_id + ", Member_id=" + Member_id+  '}';
    }
    
    
}
