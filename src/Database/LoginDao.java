/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Panu Rungkaew
 */
public class LoginDao {
    public  static boolean insert(int Staff_id){
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "INSERT INTO Login (\n" +
"                      Staff_id\n" +
"                  )\n" +
"                  VALUES (\n" +
"                      '%d'\n" +
"                  );"
;
            stm.execute(String.format(sql,Staff_id));
             Database.close();
                return true;
        } catch (SQLException ex) {
            Logger.getLogger(StaffDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return true;
    }


    private static Login toObject(ResultSet rs) throws SQLException {
        Login login;
        login = new Login();
        login.setLogin_id(rs.getInt("Login_id"));
        login.setLogin_in(rs.getString("Login_inDate"));
        login.setStaff_id(rs.getInt("Staff_id"));
        return login;
    }

     public static Login getLogin(int Loginid){
          String sql = "SELECT Login_id,\n" +
"                           Login_inDate,\n" +
"                           Staff_id\n" +
"                           FROM Login WHERE Login_id = %d";
         Connection conn = Database.connect();
         
        try {
            Statement stm = conn.createStatement();
            ResultSet rs = stm.executeQuery(String.format(sql, Loginid));
            if(rs.next()){
                Login login = toObject(rs);
                Database.close();
                return login;
            }
        } catch (SQLException ex) {
            Logger.getLogger(StaffDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return null;
    }

     private  static Login tologin(ResultSet rs) throws SQLException {
        Login login;
        login = new Login();
        login.setLogin_id(rs.getInt("Login_id"));
        login.setStaff_id(rs.getInt("Staff_id"));
        return login;
    }
     
     
     public static ArrayList<Login> getLogin(){
         ArrayList<Login> list = new ArrayList<Login>();
         Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "SELECT Login_id,\n" +
                            "Staff_id \n" +
"                           FROM Login";
            ResultSet rs = stm.executeQuery(sql);
            while(rs.next()){
                //System.out.println(rs.getInt("Staff_id")+" "+rs.getString("Staff_username"));
                Login login = tologin(rs);
                list.add(login);
            }
            Database.close();
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(LoginDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return null;
    }
}
