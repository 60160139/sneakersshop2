/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Panu Rungkaew
 */
public class BillDetailDao {
    public static  boolean insert(Product produt,int billId){
        
         Connection conn = Database.connect();
         try {
             
                    Statement stm = conn.createStatement();
                     String sql = "INSERT INTO Bucket (\n" +
"                      Bucket_number,\n" +
"                      Bucket_totalcost,\n" +
"                      Product_id,\n" +
"                      Bill_id\n" +
"                  )\n" +
"                  VALUES (\n" +
"                      '%d',\n" +
"                      '%d',\n" +
"                      '%d',\n" +
"                      '%d'\n" +
"                  );"
                             ;
                         stm.execute(String.format(sql,
                                 produt.getProduct_number(),
                                 produt.getProduct_number()*produt.getProduct_cost(),
                                 produt.getProduct_id(),
                                 billId));
                     
             Database.close();
         } catch (SQLException ex) {
             Logger.getLogger(BillDao.class.getName()).log(Level.SEVERE, null, ex);
         }
          Database.close();
        return true;
     }
    
            public static ArrayList<BillDetail> getBillDetails(){
            ArrayList<BillDetail> list = new ArrayList<BillDetail>();
         Connection conn = Database.connect();
         try {
             Statement stm = conn.createStatement();
             String sql = "SELECT Bucket_id,\n" +
"                           Bucket_number,\n" +
"                           Bucket_totalcost,\n" +
"                           Bill_id,\n" +
"                           Product_id\n" +
"                           FROM Bucket";
             ResultSet rs = stm.executeQuery(sql);
            while(rs.next()){
                //System.out.println(rs.getInt("Staff_id")+" "+rs.getString("Staff_username"));
                BillDetail billDetail = toObject(rs);
                list.add(billDetail);
            }
            Database.close();
            return list;
         } catch (SQLException ex) {
             Logger.getLogger(BillDao.class.getName()).log(Level.SEVERE, null, ex);
         }
         Database.close();
        return null;
        }
            
            public static ArrayList<BillDetail> getBillDetails(int id){
            ArrayList<BillDetail> list = new ArrayList<BillDetail>();
         Connection conn = Database.connect();
         try {
             Statement stm = conn.createStatement();
             String sql ="SELECT Bucket_id,\n" +
"                           Bucket_number,\n" +
"                           Bucket_totalcost,\n" +
"                           Bill_id,\n" +
"                           Product_id\n" +
"                       FROM Bucket\n" +
"                       WHERE Bill_id = %d";
             ResultSet rs = stm.executeQuery(String.format(sql, id));
            while(rs.next()){
                //System.out.println(rs.getInt("Staff_id")+" "+rs.getString("Staff_username"));
                BillDetail billDetail = toObject(rs);
                list.add(billDetail);
            }
            Database.close();
            return list;
         } catch (SQLException ex) {
             Logger.getLogger(BillDao.class.getName()).log(Level.SEVERE, null, ex);
         }
         Database.close();
        return null;
        }
            

    private static BillDetail toObject(ResultSet rs) throws SQLException {
        BillDetail billDetail;
        billDetail = new BillDetail();
        billDetail.setBucket_id(rs.getInt("Bucket_id"));
        billDetail.setBucket_number(rs.getInt("Bucket_number"));
        billDetail.setBucket_totolcost(rs.getInt("Bucket_totalcost"));
        billDetail.setBill_id(rs.getInt("Bill_id"));
        billDetail.setProduct_id(rs.getInt("Product_id"));
        
        
        return billDetail;
    }
    public static BillDetail getBillDetail(int Billid){
        String sql = "SELECT Bucket_id,\n" +
"                           Bucket_number,\n" +
"                           Bucket_totalcost,\n" +
"                           Bill_id,\n" +
"                           Product_id\n" +
"                       FROM Bucket\n" +
"                       WHERE Bill_id = %d";
        Connection conn = Database.connect();
         try {
             Statement stm = conn.createStatement();
             ResultSet rs = stm.executeQuery(String.format(sql, Billid));
              if(rs.next()){
                BillDetail billDetail = toObject(rs);
                Database.close();
                return billDetail;
            }
         } catch (SQLException ex) {
             Logger.getLogger(BillDetailDao.class.getName()).log(Level.SEVERE, null, ex);
         }
           Database.close();
        return null;
    }

}
