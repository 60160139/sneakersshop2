/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database;

/**
 *
 * @author Panu Rungkaew
 */
public class Product {
    int Product_id;
    String Product_type;
    String Product_brand;
    String Product_name;
    double Product_size;
    String Product_color;
    int Product_cost;
    int Product_quantity;

    public Product() {
        this.Product_id = -1;
    }

    public Product(int Product_id, String Product_type, String Product_brand, String Product_name, double Product_size, String Product_color, int Product_cost, int Product_quantity) {
        this.Product_id = Product_id;
        this.Product_type = Product_type;
        this.Product_brand = Product_brand;
        this.Product_name = Product_name;
        this.Product_size = Product_size;
        this.Product_color = Product_color;
        this.Product_cost = Product_cost;
        this.Product_quantity = Product_quantity;
    }

    public Product(int Product_id, String Product_type, String Product_brand, String Product_name, double Product_size, String Product_color, int Product_cost, int Product_quantity, int number) {
        this.Product_id = Product_id;
        this.Product_type = Product_type;
        this.Product_brand = Product_brand;
        this.Product_name = Product_name;
        this.Product_size = Product_size;
        this.Product_color = Product_color;
        this.Product_cost = Product_cost;
        this.Product_quantity = Product_quantity;
        this.Product_number = Product_number;
    }

    public int getProduct_id() {
        return Product_id;
    }

    public void setProduct_id(int Product_id) {
        this.Product_id = Product_id;
    }

    public String getProduct_type() {
        return Product_type;
    }

    public void setProduct_type(String Product_type) {
        this.Product_type = Product_type;
    }

    public String getProduct_brand() {
        return Product_brand;
    }

    public void setProduct_brand(String Product_brand) {
        this.Product_brand = Product_brand;
    }

    public String getProduct_name() {
        return Product_name;
    }

    public void setProduct_name(String Product_name) {
        this.Product_name = Product_name;
    }

    public double getProduct_size() {
        return Product_size;
    }

    public void setProduct_size(double Product_size) {
        this.Product_size = Product_size;
    }

    public String getProduct_color() {
        return Product_color;
    }

    public void setProduct_color(String Product_color) {
        this.Product_color = Product_color;
    }

    public int getProduct_cost() {
        return Product_cost;
    }

    public void setProduct_cost(int Product_cost) {
        this.Product_cost = Product_cost;
    }

    public int getProduct_quantity() {
        return Product_quantity;
    }

    public void setProduct_quantity(int Product_quatity) {
        this.Product_quantity = Product_quatity;
    }
    
     @Override
    public String toString() {
        return "Product{" + "Product_id=" + Product_id +", Product_type=" + Product_type +", Product_brand=" + Product_brand +", Product_name=" + Product_name + ", Product_size" + Product_size + ", Product_color=" + Product_color + ", Product_cost=" + Product_cost + ", Product_quantity=" + Product_quantity + ",Product_number="+ Product_number+ '}';
    }
    
        int Product_number=0;

    public int getProduct_number() {
        return Product_number;
    }

    public void setNumber(int number) {
        this.Product_number = number;
    }

    
    
}
