/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Panu Rungkaew
 */
public class MemberDao {
     public  static boolean insert(Member member){
         Connection conn = Database.connect();
         try {
             Statement stm = conn.createStatement();
             String sql = "INSERT INTO Member (\n" +
                     "                       Member_name,\n" +
                     "                       Member_surname,\n" +
                     "                       Member_age,\n" +
                     "                       Member_sex,\n" +
                     "                       Member_idcard,\n" +
                     "                       Member_tel\n" +
"                   )\n" +
"                   VALUES (\n" +
"                       '%s',\n" +
"                       '%s',\n" +
"                       %d,\n" +
"                       '%s',\n" +
"                      '%s',\n" +
"                       '%s'\n" +
"                   );"
                     ;
             /*System.out.println(String.format(sql, member.getMember_name(),member.getMember_surname()
              ,member.getMember_age(),member.getMember_sex(),member.getMember_idcard(),member.getMember_tel()));*/
              stm.execute(String.format(sql, member.getMember_name(),member.getMember_surname()
              ,member.getMember_age(),member.getMember_sex(),member.getMember_idcard(),member.getMember_tel()));
              Database.close();
         } catch (SQLException ex) {
             Logger.getLogger(MemberDao.class.getName()).log(Level.SEVERE, null, ex);
         }
          Database.close();
          return true;
     }
      public static ArrayList<Member> getMembers(){
         ArrayList<Member> list = new ArrayList<Member>();
         Connection conn = Database.connect();
         try {
             Statement stm = conn.createStatement();
             String sql = "SELECT Member_id,\n" +
"                                 Member_name,\n" +
"                                 Member_surname,\n" +
"                                 Member_age,\n" +
"                                 Member_sex,\n" +
"                                 Member_idcard,\n" +
"                                 Member_tel\n" +
"                                 FROM Member";
          ResultSet rs = stm.executeQuery(sql);
          while(rs.next()){    
              Member member = toObject(rs);
              list.add(member);
          }
          Database.close();
            return list;
         } catch (SQLException ex) {
             Logger.getLogger(MemberDao.class.getName()).log(Level.SEVERE, null, ex);  
         }
         Database.close();
        return null;
      }

    private static Member toObject(ResultSet rs)throws SQLException  {
        Member member;
        member = new Member();
        member.setMember_id(rs.getInt("Member_id"));
        member.setMember_name(rs.getString("Member_name"));
        member.setMember_surname(rs.getString("Member_surname"));
        member.setMember_age(rs.getInt("Member_age"));
        member.setMember_sex(rs.getString("Member_sex"));
        member.setMember_idcard(rs.getString("Member_idcard"));
        member.setMember_tel(rs.getString("Member_tel"));
        
        return member;
    }
     public static Member getMember(int Memberid){
          String sql = "SELECT Member_id,\n" +
"                                 Member_name,\n" +
"                                 Member_surname,\n" +
"                                 Member_age,\n" +
"                                 Member_sex,\n" +
"                                 Member_idcard,\n" +
"                                 Member_tel\n" +
"                                 FROM Member WHERE Member_id = %d";
          Connection conn = Database.connect();
         try {
             Statement stm = conn.createStatement();
              ResultSet rs = stm.executeQuery(String.format(sql, Memberid));
              if(rs.next()){
                Member member = toObject(rs);
                Database.close();
                return member;
              }
         } catch (SQLException ex) {
             Logger.getLogger(MemberDao.class.getName()).log(Level.SEVERE, null, ex);
         }
         Database.close();
         return null;
     }
          public static boolean ChackMember(int Memberid){
            String sql = "SELECT Member_id\n"
                    + "FROM Member WHERE Member_id = %d";
          Connection conn = Database.connect();
         try {
             Statement stm = conn.createStatement();
              ResultSet rs = stm.executeQuery(String.format(sql, Memberid));
              if(rs.next()){
                Member member = toObject(rs);
                Database.close();
                return true;
              }
         } catch (SQLException ex) {
             Logger.getLogger(MemberDao.class.getName()).log(Level.SEVERE, null, ex);
         }
         Database.close();
         return false;
     }
     
     public  static boolean update(Member member){
         String sql = "UPDATE Member\n" +
"   SET Member_name = '%s',\n" +
"       Member_surname = '%s',\n" +
"       Member_age = %d,\n" +
"       Member_sex = '%s',\n" +
"       Member_idcard = '%s',\n" +
"       Member_tel = '%s'\n" +
" WHERE Member_id = %d;";
         Connection conn = Database.connect();
         Statement stm;
         try {
             stm = conn.createStatement();
             boolean ret = stm.execute(String.format(sql, member.getMember_name()
                     ,member.getMember_surname()
                     ,member.getMember_age()
                     ,member.getMember_sex()
                     ,member.getMember_idcard()
                     ,member.getMember_tel()
                     ,member.getMember_id()
                      ));
                    Database.close();
                    return ret;
         } catch (SQLException ex) {
             Logger.getLogger(MemberDao.class.getName()).log(Level.SEVERE, null, ex);
         }
         Database.close();
        return true;
     }
      public static boolean delete(Member member){
          String sql = "DELETE FROM Member   WHERE  Member_id = %d " ;
          Connection conn = Database.connect();
         try {
             Statement stm = conn.createStatement();
             boolean ret = stm.execute(String.format(sql,member.getMember_id()));
             Database.close();
         } catch (SQLException ex) {
             Logger.getLogger(MemberDao.class.getName()).log(Level.SEVERE, null, ex);
         }
         Database.close();
         return true;
      }
      
      
}
