/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sneakersshop2.Ui;

import Database.Member;
import Database.Staff;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Panu Rungkaew
 */
public class MemberTableModel extends AbstractTableModel{
     ArrayList<Member> MemberList = new ArrayList<Member>();
    String[] columnNames ={"IDMember","Name","Surname","Age","Sex","IDcard","Tel."};

    @Override
    public int getRowCount() {
        return MemberList.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Member member = MemberList.get(rowIndex);
        switch(columnIndex){
            case 0 : return member.getMember_id();
            case 1 : return member.getMember_name();
            case 2 : return member.getMember_surname();
            case 3 : return member.getMember_age();
            case 4 : return member.getMember_sex();
            case 5 : return member.getMember_idcard();
            case 6 : return member.getMember_tel();
        }
        return "";
    }
    public void setData(ArrayList<Member> memberList){
        this.MemberList = memberList;
        fireTableDataChanged();         
    }
     @Override
    public String getColumnName(int column) {
        return columnNames[column]; //To change body of generated methods, choose Tools | Templates.
    }
}
