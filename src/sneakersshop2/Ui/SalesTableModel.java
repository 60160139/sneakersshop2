/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sneakersshop2.Ui;

import Database.BillDetail;
import Database.Member;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Panu Rungkaew
 */
public class SalesTableModel extends AbstractTableModel{
        ArrayList<BillDetail> BillList = new ArrayList<BillDetail>();
        String[] columnNames ={"ID_Bill","ID_Bucket","ID_Product","Number","Price"};

    @Override
    public int getRowCount() {
       return BillList.size(); 
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        BillDetail billDetail = BillList.get(rowIndex);
        switch(columnIndex){
            case 0 : return billDetail.getBill_id();
            case 1 : return billDetail.getBucket_id();
            case 2 : return billDetail.getProduct_id();
            case 3 : return billDetail.getBucket_number();
            case 4 : return billDetail.getBucket_totolcost();
        }
        return "";
    }
        public void setData(ArrayList<BillDetail> billDetail){
        this.BillList = billDetail;
        fireTableDataChanged();         
    }
     @Override
    public String getColumnName(int column) {
        return columnNames[column]; //To change body of generated methods, choose Tools | Templates.
    }
    
}
