/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sneakersshop2.Ui;

import Database.Product;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Panu Rungkaew
 */
public class ChackBillTableModel extends AbstractTableModel{
    
    
    ArrayList<Product> productlist = new ArrayList<Product>();
    String[] columnNames ={"IDProduct","Type","Brand","Name","Size","Color","Cost","Status","Amount","Totalcost"};
   

    @Override
    public int getRowCount() {
        return productlist.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Product product = productlist.get(rowIndex);
        switch(columnIndex){
            case 0 : return product.getProduct_id();
            case 1 : return product.getProduct_type();
            case 2 : return product.getProduct_brand();
            case 3 : return product.getProduct_name();
            case 4 : return product.getProduct_size();
            case 5 : return product.getProduct_color();
            case 6 : return product.getProduct_cost();
            case 7 : return getStatus(product);
            case 8 : return product.getProduct_number();
            case 9 : return product.getProduct_cost()*product.getProduct_number();
        }
        return "";
    }
    
    private  String getStatus(Product product){
        
        if(product.getProduct_quantity()>product.getProduct_number()){
            return "Ready";
        }else{
            return "UnReady";
        }
    }
    public void setData(ArrayList<Product> productslist){
        this.productlist = productslist;
        fireTableDataChanged();
                
    }
    @Override
    public String getColumnName(int column) {
        return columnNames[column]; //To change body of generated methods, choose Tools | Templates.
    }
}
