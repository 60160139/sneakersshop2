/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sneakersshop2.Ui;


import Database.Staff;
import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;/*

/**
 *
 * @author Panu Rungkaew
 */
public class StaffTableModel extends AbstractTableModel{
    ArrayList<Staff> staffList = new ArrayList<Staff>();
     String[] columnNames ={"IDStaff","Name","Surname","Age","Sex","IDcard","Tel.","Type"};
    
    @Override
    public int getRowCount() {
        return staffList.size();
    }

    public int getColumnCount() {
        return columnNames.length;
     }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Staff staff = staffList.get(rowIndex);
        switch(columnIndex){
            case 0 : return staff.getStaff_id();
            case 1 : return staff.getStaff_name();
            case 2 : return staff.getStaff_surname();
            case 3 : return staff.getStaff_age();
            case 4 : return staff.getStaff_sex();
            case 5 : return staff.getStaff_idcard();
            case 6 : return staff.getStaff_tel();
            case 7 : return staff.getStaff_type();
        }
        return "";
    }
    public void setData(ArrayList<Staff> staffList){
        this.staffList = staffList;
        fireTableDataChanged();
                
    }

    @Override
    public String getColumnName(int column) {
        return columnNames[column]; //To change body of generated methods, choose Tools | Templates.
    }
  
}
