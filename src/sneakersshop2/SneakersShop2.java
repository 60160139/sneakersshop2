/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sneakersshop2;

import Database.Member;
import Database.MemberDao;
import Database.Product;
import Database.ProductDao;
import java.sql.Connection;

/**
 *
 * @author Panu Rungkaew
 */
public class SneakersShop2 {
     static String url = "jdbc:sqlite:./db/sneakersshop.db";
     static Connection conn = null;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
      
        Product member = new Product();
        //member.setProduct_id(-1);
        member.setProduct_type("asad");
        member.setProduct_brand("asdasd");
        member.setProduct_name("men");
        member.setProduct_size(20);
        member.setProduct_color("789456123");
        member.setProduct_cost(3000);
        member.setProduct_quantity(30);
        
        ProductDao.update(member);
        
    }
    
}
